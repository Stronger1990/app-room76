$(document).ready(function () {

    var $buttons = $(".modal-image");
    var $buttonClose = $(".close");
    var $modalWindow = $(".modalwindow");
    var $layout = $(".overlay");
    var $body = $("body");


    //Open modal
    $buttons.click(function (e) {
        e.preventDefault();
        $layout.addClass("visible");
        $modalWindow.addClass("visible");
        $body.addClass("no-scroll");
    });

    //Close modal
    $buttonClose.click(function (e) {
        e.preventDefault();
        $layout.removeClass("visible");
        $modalWindow.removeClass("visible");
        $body.removeClass("no-scroll");
    });

    //Close widget when clicking outside the area
    $(document).mouseup(function (e) {
        if (!$modalWindow.is(e.target) &&
            $modalWindow.has(e.target).length === 0) {}
    });
});
