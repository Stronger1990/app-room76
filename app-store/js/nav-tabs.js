$(document).ready(function () {

    $(".tab_item").not(":first").hide();

    $(".tab-description .tab").click(function () {
        $(".tab-description .tab").removeClass("active")
            .eq($(this).index()).addClass("active");

        $(".tab_item").hide()
            .eq($(this).index()).fadeIn()
    }).eq(0).addClass("active");

    $(".btn-height").click(function () {
        $(".tab_content").toggleClass("height-auto")
        $(".tab_item").toggleClass("height-auto")
    });
});
